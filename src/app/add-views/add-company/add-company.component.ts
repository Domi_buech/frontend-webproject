import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Company } from '../../templates/company.template'
import { User } from '../../templates/user.template'
import { ServiceContract } from '../../templates/serviceContract.template'

import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css']
})
export class AddCompanyComponent implements OnInit {

  company: Company;

  userList: User;
  selectedUsers: User[]

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.company = new Company()
    this.company.name = ''
    this.loadAllUsers()
    console.log('Company-Detail: ngOnInit()')
  }

  loadAllUsers() {
    console.log('UsersComponent loading data');
    this.apiService.getAllUsers().then(
      apiValue => {
        this.userList = apiValue
        console.log(this.userList)
      }

    ).catch(error => {
      console.log('UsersComponent loading FAILED')
      console.log(error)
      alert('Loading users failed, please try again or reload')
    })
  }


  //post admin needs to be set manually
  createCompany() {
    this.company.tusers = this.selectedUsers
    console.log(this.company)
    this.apiService.updateCompany(this.company).then(
      apiValue => {
        console.log('updateCompany: ' + apiValue)
        console.log('Successfully saved company data in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of copmany failed, please try again')
    })
  }

}
