import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { License } from '../../templates/license.template'
import { Product } from '../../templates/product.template'
import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-add-license',
  templateUrl: './add-license.component.html',
  styleUrls: ['./add-license.component.css']
})
export class AddLicenseComponent implements OnInit {

  license: License;
  product: Product;
  productSaved: Product

  constructor(
    private apiService: ApiService, 
    private route: ActivatedRoute, private router: Router, 
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.license = new License()
    this.product = new Product()
    console.log(this.license)
    console.log('Add-License: ngOnInit()')
  }

  async createLicense() {
    //create product and get PID to assign it to the license
    await this.createProduct()
    this.license.tproduct = this.productSaved
    console.log(this.license.oid)
    var myDate = Date.parse(this.license.expire)
    console.log(myDate)
    var apiLicense = new License()
    apiLicense = this.license
    console.log(apiLicense)
    await this.apiService.updateLicense(apiLicense).then(
      apiValue => {
        console.log('updateLicense: ' + apiValue)
        console.log('Successfully created license in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of license failed, please try again')
    })
    const options = { year: 'numeric', month: 'long', day: 'numeric'};
    var expireDate = new Date(this.license.expire).toLocaleDateString('de-DE', options)
    this.license.expire = expireDate
  }

  async createProduct() {
    await this.apiService.updateProduct(this.product).then(
      apiValue => {
        console.log(apiValue)
        this.productSaved = apiValue
        console.log('Successfully created product in backend')
      }
    ).catch(error => {
      console.log('Failed saving product')
      console.log(error)
      alert('Saving of product failed, please try again')
    })
  }

}
