import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ServiceContract } from '../../templates/serviceContract.template'
import { Company } from '../../templates/company.template'
import { License } from '../../templates/license.template'
import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-add-service-contract',
  templateUrl: './add-service-contract.component.html',
  styleUrls: ['./add-service-contract.component.css']
})
export class AddServiceContractComponent implements OnInit {

  sercon: ServiceContract
  serconSaved: ServiceContract

  companies: Company[]
  licenseList: License[]

  selectedCompany: Company;
  selectedLicenses: License[];


  constructor(
    private apiService: ApiService, 
    private route: ActivatedRoute, 
    private router: Router, 
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.sercon = new ServiceContract()
    console.log('ServiceContract-Detail: ngOnInit()')
    this.loadAllCompanies()
    this.loadAllLicenses()
  }

  async loadAllCompanies() {
    console.log('CompaniesComponent loading data');
    await this.apiService.getAllCompanies().then(
      apiValue => {
          this.companies = apiValue
        }
      
    ).catch(error => {
      console.log('CompaniesComponent loading FAILED')
      console.log(error)
    })
  }

  loadAllLicenses() {
    console.log('LicensesComponent loading data');
    this.apiService.getAllLicenses().then(
      apiValue => {
          this.licenseList = apiValue
          console.log(this.licenseList)
        }
      
    ).catch(error => {
      console.log('LicensesComponent loading FAILED')
      console.log(error)
      alert('Loading licenses failed, please try again or reload')
    })
  }


  async createServiceContract() {
    console.log(this.sercon.oid)
    console.log(this.selectedCompany)
    this.sercon.tcompany = this.selectedCompany

    await this.apiService.updateServiceContract(this.sercon).then(
      apiValue => {
        console.log('updateServiceContract: ' + apiValue)
        this.serconSaved = apiValue
        this.updateLicenses()
        console.log('Successfully saved Service Contract data in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of Service Contract failed, please try again')
    })
     const options = { year: 'numeric', month: 'long', day: 'numeric'};
            var startDate = new Date(this.sercon.start).toLocaleDateString('de-DE', options)
            var endDate = new Date(this.sercon.end).toLocaleDateString('de-DE', options)
            this.sercon.start = startDate
            this.sercon.end = endDate
  }


  // updates association from licenses to created service contract and company
  async updateLicenses() {
    for(let license of this.selectedLicenses) {
      console.log(license)
      if(this.serconSaved) license.tservicecontract_OID = this.serconSaved.oid
      if(this.selectedCompany) license.tservicecontract_TCOMPANY_OID = this.selectedCompany.oid

      const options = {year: 'numeric', month: 'long', day: 'numeric' };
      var testDate = new Date(license.expire).toLocaleDateString('de-DE', options)
      license.expire = testDate

      await this.apiService.updateLicense(license).then(
        apiValue => {
          console.log('updateLicense: ' + apiValue)
          console.log('Successfully updated license data in backend')
        }
      ).catch(error => {
        console.log('Failed saving data')
        console.log(error)
        alert('Updating of license failed, please try again')
      })
    }

  }

}
