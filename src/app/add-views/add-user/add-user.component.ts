import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../../templates/user.template'
import { Company } from '../../templates/company.template'
import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  user: User;

  selectedCompany: Company;
  companies: Company[]

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.user = new User();
    this.user.loginname = ''
    console.log('addUser: ngOnInit()')
    this.loadAllCompanies()

  }


  async loadAllCompanies() {
    console.log('CompaniesComponent loading data');
    await this.apiService.getAllCompanies().then(
      apiValue => {
        this.companies = apiValue
      }

    ).catch(error => {
      console.log('CompaniesComponent loading FAILED')
      console.log(error)
    })
  }

  createUser() {
    // converts from boolean to int for backend
    this.user.admin = this.user.admin ? 1 : 0
    this.user.active = this.user.active ? 1 : 0

    console.log(this.selectedCompany)
    this.user.tcompany = this.selectedCompany
    var apiUser = new User()
    apiUser = this.user
    this.apiService.updateUser(this.user).then(
      apiValue => {
        console.log('createUser: ' + apiValue)
        console.log('Successfully create user in backend')
      }
    ).catch(error => {
      console.log('Failed creating user')
      console.log(error)
      alert('Saving the new user failed, please try again')
    })

  }

}
