import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { NgModule } from '@angular/core';
import { CustomMaterialModule } from './core/material.module';
import { AppRoutingModule } from './core/app.routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { UsersComponent } from './list-views/users/users.component';
import { ServicecontractsComponent } from './list-views/servicecontracts/servicecontracts.component';
import { CompaniesComponent } from './list-views/companies/companies.component';
import { LicensesComponent } from './list-views/licenses/licenses.component';
import { ProfileViewComponent } from './detail-views/profile-view/profile-view.component';
import { ServiceContractDetailComponent } from './detail-views/service-contract-detail/service-contract-detail.component';
import { LicenseDetailComponent } from './detail-views/license-detail/license-detail.component';
import { CompanyDetailComponent } from './detail-views/company-detail/company-detail.component';
import { HomeComponent } from './home/home.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RegisterComponent } from './register/register.component';
import { AddUserComponent } from './add-views/add-user/add-user.component';
import { AddCompanyComponent } from './add-views/add-company/add-company.component';
import { AddLicenseComponent } from './add-views/add-license/add-license.component';
import { AddServiceContractComponent } from './add-views/add-service-contract/add-service-contract.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    ServicecontractsComponent,
    CompaniesComponent,
    LicensesComponent,
    ProfileViewComponent,
    ServiceContractDetailComponent,
    LicenseDetailComponent,
    CompanyDetailComponent,
    HomeComponent,
    ToolbarComponent,
    RegisterComponent,
    AddUserComponent,
    AddCompanyComponent,
    AddLicenseComponent,
    AddServiceContractComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
