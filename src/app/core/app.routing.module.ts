import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from '../register/register.component';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';

import { ProfileViewComponent } from '../detail-views/profile-view/profile-view.component';
import { CompanyDetailComponent } from '../detail-views/company-detail/company-detail.component';
import { LicenseDetailComponent } from '../detail-views/license-detail/license-detail.component';
import { ServiceContractDetailComponent } from '../detail-views/service-contract-detail/service-contract-detail.component';

import { LicensesComponent } from '../list-views/licenses/licenses.component';
import { UsersComponent } from '../list-views/users/users.component';
import { CompaniesComponent } from '../list-views/companies/companies.component';
import { ServicecontractsComponent } from '../list-views/servicecontracts/servicecontracts.component';

import { AddUserComponent } from '../add-views/add-user/add-user.component';
import { AddCompanyComponent } from '../add-views/add-company/add-company.component';
import { AddLicenseComponent } from '../add-views/add-license/add-license.component';
import { AddServiceContractComponent } from '../add-views/add-service-contract/add-service-contract.component';



const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'users', component: UsersComponent },
  { path: 'companies', component: CompaniesComponent },
  { path: 'sercons', component: ServicecontractsComponent },
  { path: 'licenses', component: LicensesComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileViewComponent },
  { path: 'profile/:oid', component: ProfileViewComponent },

  { path: 'users/addUser', component: AddUserComponent },
  { path: 'companies/addCompany', component: AddCompanyComponent },
  { path: 'licenses/addLicense', component: AddLicenseComponent },
  { path: 'sercons/addServiceContract', component: AddServiceContractComponent },

  { path: 'companies/:oid', component: CompanyDetailComponent },
  {
    path: 'licenses/:oid',
    component: LicenseDetailComponent
  },
  { path: 'sercons/:oid', component: ServiceContractDetailComponent },
  // { path: 'profile', redirectTo: '/login', pathMatch: 'full'},
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }