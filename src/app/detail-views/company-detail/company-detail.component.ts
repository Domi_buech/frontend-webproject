import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Company } from '../../templates/company.template'
import { ServiceContract } from '../../templates/serviceContract.template'
import { User } from '../../templates/user.template'
import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.css']
})

export class CompanyDetailComponent implements OnInit {

  company: Company;

  userList: User[];
  preEmployees: User[] = []
  postEmployees: User[] = []

  serconList: ServiceContract[] = []
  preSercons: ServiceContract[] = []
  postSercons: ServiceContract[] = []


  compareFunction = (o1: any, o2: any) => o1.oid == o2.oid;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    console.log('Company-Detail: ngOnInit()')
    this.company = new Company()
    this.company.name = ''
    this.loadData()
    this.loadAllUsers()
    this.loadAllServiceContracts()
  }

  async loadData() {
    if (!!this.authService.isAdmin) {
      var oid = +this.route.snapshot.paramMap.get('oid');
      try {
        await this.apiService.getCompanyById(oid).then(
          apiValue => {
            this.company = apiValue.company
            // this.loadCompanyEmployees()
            // this.loadCompanyServiceContracts()
            console.log(this.company)
            console.log(this.userList)
          }
        )
      } catch (error) {
        console.log('Error loading company data')
        console.log(error)
      }

    } else {
      console.log('no Admin')
      alert('You are not an admin, get out!')
      this.router.navigate(['/home'])
    }

  }


  //post admin needs to be set manually
  updateCompany() {
    console.log(this.company.oid)
    this.company.tusers = this.postEmployees
    this.company.tservicecontracts = this.postSercons
    console.log(this.company)
    this.apiService.updateCompany(this.company).then(
      apiValue => {
        console.log('updateCompany: ' + apiValue)
        console.log('Successfully saved company data in backend')


        // set old employees company to null
        for (let oldEmployee of this.preEmployees) {
          oldEmployee.tcompany = null
          this.updateUserCompany(oldEmployee)
          this.sleep(100)
        }

        // set new employee company to current company
        for (let postEmployee of this.postEmployees) {
          postEmployee.tcompany = this.company
          this.updateUserCompany(postEmployee)
          this.sleep(100)
        }

        // set old employees company to null
        for (let oldSercon of this.preSercons) {
          oldSercon.tcompany = null
          this.updateServiceContractCompany(oldSercon)
          this.sleep(100)
        }

        // set new employee company to current company
        for (let postSercon of this.postSercons) {
          postSercon.tcompany = this.company
          this.updateServiceContractCompany(postSercon)
          this.sleep(100)
        }

      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of copmany failed, please try again')
    })
  }

  loadAllUsers() {
    console.log('UsersComponent loading data');
    this.apiService.getAllUsers().then(
      apiValue => {
        this.userList = apiValue
        // checks if user belongs to company
        for (let user of this.userList) {
          if (user.tcompany == null) {
            user.tcompany = new Company()
            console.log('if')
          } else if (user.tcompany.oid == this.company.oid) {
            this.preEmployees.push(user)
            console.log('else if')
          }
        }
        this.postEmployees = this.preEmployees
        console.log(this.postEmployees)
      }

    ).catch(error => {
      console.log('UsersComponent loading FAILED')
      console.log(error)
      alert('Loading users failed, please try again or reload')
    })
  }


  loadAllServiceContracts() {
    console.log('ServiceContractComponent loading data');
    this.apiService.getAllServiceContracts().then(
      apiValue => {
        this.serconList = apiValue
        console.log(this.serconList)
        // checks if sercon belongs to company
        for (let sercon of this.serconList) {
          if (sercon.tcompany == null) {
            sercon.tcompany = new Company()
            console.log('if')
          } else if (sercon.tcompany.oid == this.company.oid) {
            this.preSercons.push(sercon)
            console.log('else if')
          }
        }
        this.postSercons = this.preSercons
      }

    ).catch(error => {
      console.log('ServiceContractComponent loading FAILED')
      console.log(error)
      alert('Loading Service contracts failed, please try again or reload')
    })
  }


  async loadCompanyEmployees() {
    console.log('loadCompanyEmployees loading data');
    this.apiService.getCompanyEmployees(this.company.oid).then(
      apiValue => {
        this.preEmployees = apiValue
        this.postEmployees = apiValue
        console.log(this.postEmployees)
      }
    ).catch(error => {
      console.log('loadCompanyEmployees FAILED')
      console.log(error)
      alert('Loading employees failed, please try again or reload')
    })
  }


  async loadCompanyServiceContracts() {
    console.log('loadCompanyServiceContracts loading data');
    this.apiService.getCompanyServiceContracts(this.company.oid).then(
      apiValue => {
        this.preSercons = apiValue
        this.postSercons = apiValue
        console.log(this.postEmployees)
      }
    ).catch(error => {
      console.log('loadCompanyServiceContracts FAILED')
      console.log(error)
      alert('Loading service contracts failed, please try again or reload')
    })
  }


  updateUserCompany(employee) {
    // converts from boolean to int for backend
    employee.admin = employee.admin ? 1 : 0
    employee.active = employee.active ? 1 : 0

    if (employee.tcompany) employee.tcompany.tusers = []
    if (employee.tcompany) employee.tcompany.tservicecontracts = []
    this.apiService.updateUser(employee).then(
      apiValue => {
        console.log('updateUserCompany: ' + apiValue)
        console.log('Successfully saved user data in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of new companyoid, please re-login')
    })
  }

  updateServiceContractCompany(sercon) {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    sercon.start = new Date(sercon.start).toLocaleDateString('de-DE', options)
    sercon.end = new Date(sercon.end).toLocaleDateString('de-DE', options)

    if (sercon.tcompany) sercon.tcompany.tservicecontracts = []
    if (sercon.tcompany) sercon.tcompany.tusers = []
    console.log(sercon)
    this.apiService.updateServiceContract(sercon).then(
      apiValue => {
        console.log('updateServiceContract: ' + apiValue)
        console.log('Successfully saved user data in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of new ServiceContract, please re-login')
    })
  }

  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }


}
