import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { License } from '../../templates/license.template'
import { Product } from '../../templates/product.template'
import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-license-detail',
  templateUrl: './license-detail.component.html',
  styleUrls: ['./license-detail.component.css']
})
export class LicenseDetailComponent implements OnInit {

  license: License;

  constructor(
    private apiService: ApiService, 
    private route: ActivatedRoute, private router: Router, 
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.license = new License()
    this.license.tproduct = new Product()
    console.log('License-Detail: ngOnInit()')
    this.loadData()
  }

  async loadData() {
    if (this.authService.isAdmin) {
      var oid = +this.route.snapshot.paramMap.get('oid');
      try {
        await this.apiService.getLicenseById(oid).then(
          apiValue => {
            this.license = apiValue.license
            if (this.license.tproduct == null) this.license.tproduct = new Product()
            console.log(this.license)
            const options = { year: 'numeric', month: 'long', day: 'numeric' };
            var testDate = new Date(this.license.expire).toLocaleDateString('de-DE', options)
            this.license.expire = testDate
          }
        )
      } catch (error) {
        console.log('Error loading license data')
        console.log(error)
      }

    } else {
      console.log('no Admin')
      alert('You are not an admin, get out!')
      this.router.navigate(['/home'])
    }
  }

  async updateLicense() {
    console.log(this.license.oid)
    var myDate = Date.parse(this.license.expire)
    console.log(myDate)
    await this.apiService.updateLicense(this.license).then(
      apiValue => {
        console.log('updateLicense: ' + apiValue)
        console.log('Successfully saved license data in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of license failed, please try again')
    })
    const options = { year: 'numeric', month: 'long', day: 'numeric'};
    var expireDate = new Date(this.license.expire).toLocaleDateString('de-DE', options)
    this.license.expire = expireDate
  }

}
