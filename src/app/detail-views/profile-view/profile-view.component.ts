import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../../templates/user.template'
import { Company } from '../../templates/company.template'
import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})

export class ProfileViewComponent implements OnInit {

  user: User;

  isAdmin: boolean = false;
  selectedCompany: Company;
  companies: Company[]
  updatingOwnProfile: boolean = false;

  company: string;
  department: string;
  currentUser: any

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.user = new User();
    this.user.loginname = ''
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    console.log('profileview: ngOnInit()')
    this.loadAllCompanies()

    if (this.router.url == '/profile') {
      this.user.loginname = ''
      this.updatingOwnProfile = true
    } else {
      this.updatingOwnProfile = false
    }
    this.loadData()

  }


  // checks if oid is in the given route, if yes admin Status is checked
  // if oid is not a route paramter, it's extracted off the "currentUser" item from local storage
  // then calls the api and database or throws error
  async loadData() {
    var oid = null
    var companyOid = null

    oid = +this.route.snapshot.paramMap.get('oid');
    companyOid = +this.route.snapshot.paramMap.get('companyOid');
    // if not there, sets variables = 0

    if (oid == 0 || oid == null) { // if true, user wants to edit own profile
      if (this.currentUser.oid == 0 || this.currentUser.oid == null) {
        console.log('not logged in')
        alert('You are not logged in')
        this.router.navigate(['/home'])
      } else {
        oid = this.currentUser.oid
        companyOid = this.currentUser.tcompany_OID
      }
    } else {
      // do nothing, oid and companyOid were route parameter
    }

    try {
      if (companyOid == null) companyOid = 0
      this.user = await this.apiService.getUserById(oid)
      if (companyOid != 0) this.getUserCompany()
      this.selectedCompany = this.user.tcompany
      console.log(this.user)
    } catch (error) {
      console.log('Error loading user data')
      console.log(error)
    }
  }


  //post --> updates localStorage item
  updateMyProfile() {
    // converts from boolean to int for backend
    this.user.admin = this.user.admin ? 1 : 0
    this.user.active = this.user.active ? 1 : 0

    this.currentUser.oid = this.user.oid
    console.log(this.selectedCompany)
    this.user.tcompany = this.selectedCompany
    console.log(this.selectedCompany)
  
    // if a user selects a new company, the local storage item is updated
    if (this.user.tcompany != null && this.updatingOwnProfile == true) this.currentUser.tcompany_OID = this.selectedCompany.oid
    this.currentUser.adminStatus = this.user.admin
    this.authService.loggedIn(this.currentUser)

    console.log(this.user)

    this.apiService.updateUser(this.user).then(
      apiValue => {
        console.log('updateUser: ' + apiValue)
        console.log('Successfully saved user data in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of your profile failed, please re-login')
    })

  }

  //post --> updates localStorage item
  updateUserAsAdmin() {
    // converts from boolean to int for backend
    this.user.admin = this.user.admin ? 1 : 0
    this.user.active = this.user.active ? 1 : 0

    this.user.tcompany = this.selectedCompany
    console.log(this.selectedCompany)
    this.apiService.updateUser(this.user).then(
      apiValue => {
        console.log('updateUser: ' + apiValue)
        console.log('Successfully saved user data in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of your profile failed, please re-login')
    })

  }

  getUserCompany() {
    if (this.user.tcompany_OID) {
      this.apiService.getCompanyById(this.user.tcompany_OID).then(
        apiValue => {
          if (apiValue) {
            this.company = apiValue.company.name
            this.department = apiValue.company.department
          }
        }
      ).catch(error => {
        console.log('Failed Loading user company')
        console.log(error)
      })
    } else {
      console.log('User has no company')
    }
  }

  async loadAllCompanies() {
    console.log('CompaniesComponent loading data');
    await this.apiService.getAllCompanies().then(
      apiValue => {
        this.companies = apiValue
      }

    ).catch(error => {
      console.log('CompaniesComponent loading FAILED')
      console.log(error)
    })
  }


}
