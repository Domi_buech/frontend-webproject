import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceContractDetailComponent } from './service-contract-detail.component';

describe('ServiceContractDetailComponent', () => {
  let component: ServiceContractDetailComponent;
  let fixture: ComponentFixture<ServiceContractDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceContractDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceContractDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
