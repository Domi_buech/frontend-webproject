import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ServiceContract } from '../../templates/serviceContract.template'
import { Company } from '../../templates/company.template'
import { License } from '../../templates/license.template'
import { Product } from '../../templates/product.template'
import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-service-contract-detail',
  templateUrl: './service-contract-detail.component.html',
  styleUrls: ['./service-contract-detail.component.css']
})
export class ServiceContractDetailComponent implements OnInit {

  sercon: ServiceContract
  companies: Company[]
  selectedCompany: Company;

  licenseList: License[]
  preLicenses: License[] = []
  postLicenses: License[] = []


  compareFunction = (o1: any, o2: any) => o1.oid == o2.oid;

  constructor(
    private apiService: ApiService, 
    private route: ActivatedRoute, 
    private router: Router, 
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.sercon = new ServiceContract()
    console.log('ServiceContract-Detail: ngOnInit()')
    this.loadData()
    this.loadAllCompanies()
    this.loadAllLicenses()
  }


  async loadData() {
    if (this.authService.isAdmin) {
      var oid = +this.route.snapshot.paramMap.get('oid');
      // var companyOid = +this.route.snapshot.paramMap.get('tcompany_OID');
      try {
        await this.apiService.getServiceContractById(oid).then(
          apiValue => {
            this.sercon = apiValue.serviceContract
            console.log(this.sercon)
            if(this.sercon.tcompany != null) {
              this.loadCompanyByOid(this.sercon.tcompany.oid)
            } else {
            // something needs to happen
            this.sercon.tcompany = new Company()
            this.sercon.tcompany.oid = null
            }
            console.log(this.sercon)
            const options = { year: 'numeric', month: 'long', day: 'numeric'};
            var startDate = new Date(this.sercon.start).toLocaleDateString('de-DE', options)
            var endDate = new Date(this.sercon.end).toLocaleDateString('de-DE', options)
            this.sercon.start = startDate
            this.sercon.end = endDate
          }
        )
      } catch (error) {
        console.log('Error loading service contract data')
        console.log(error)
      }

    } else {
      console.log('no Admin')
      alert('You are not an admin, get out!')
      this.router.navigate(['/home'])
    }
  }

  async updateServiceContract() {
    console.log(this.sercon.oid)
    console.log(this.selectedCompany)
    // this.sercon.tcompany_OID = this.selectedCompany.oid
    this.sercon.tcompany = this.selectedCompany
    console.log(this.sercon)
    await this.apiService.updateServiceContract(this.sercon).then(
      apiValue => {
        console.log('updateServiceContract: ' + apiValue)
        console.log('Successfully saved Service Contract data in backend')

         // set old license sercon to null
         for (let oldLicense of this.preLicenses) {
          oldLicense.tservicecontract_OID = null
          this.updateLicenseServiceContract(oldLicense)
          this.sleep(100)
        }

        // set new license sercon to current sercon
        for (let postLicense of this.postLicenses) {
          postLicense.tservicecontract_OID = this.sercon.oid
          this.updateLicenseServiceContract(postLicense)
          this.sleep(100)
        }
        
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of Service Contract failed, please try again')
    })
     const options = { year: 'numeric', month: 'long', day: 'numeric'};
            var startDate = new Date(this.sercon.start).toLocaleDateString('de-DE', options)
            var endDate = new Date(this.sercon.end).toLocaleDateString('de-DE', options)
            this.sercon.start = startDate
            this.sercon.end = endDate
  }

  async loadAllCompanies() {
    console.log('CompaniesComponent loading data');
    await this.apiService.getAllCompanies().then(
      apiValue => {
          this.companies = apiValue
        }
      
    ).catch(error => {
      console.log('CompaniesComponent loading FAILED')
      console.log(error)
    })
  }

  async loadCompanyByOid(companyOid) {
    console.log('Loading company of service contract');
    try {
      await this.apiService.getCompanyById(companyOid).then(
        apiValue => {
          this.selectedCompany = apiValue.company
        }
      )
    } catch (error) {
      console.log('Error loading company data of service contract')
      console.log(error)
    }
  }


  
  loadAllLicenses() {
    console.log('LicensesComponent loading data');
    this.apiService.getAllLicenses().then(
      apiValue => {
        this.licenseList = apiValue
        console.log(this.licenseList)
        for (let license of this.licenseList) {
          if (license.tproduct == null) {
            license.tproduct = new Product()
            license.tproduct.oid = null
            console.log('if')
          }
          if (license.tservicecontract_OID == 0) {
            license.tservicecontract_OID = null
          } else if (license.tservicecontract_OID == this.sercon.oid) {
            this.preLicenses.push(license)
            console.log('else if')
          }
        }
        this.postLicenses = this.preLicenses
        console.log(this.postLicenses)
      }

    ).catch(error => {
      console.log('LicensesComponent loading FAILED')
      console.log(error)
      alert('Loading licenses failed, please try again or reload')
    })
  }

  updateLicenseServiceContract(oldLicense) {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    oldLicense.expire = new Date(oldLicense.expire).toLocaleDateString('de-DE', options)
    
    this.apiService.updateLicense(oldLicense).then(
      apiValue => {
        console.log('updateLicense: ' + apiValue)
        console.log('Successfully saved license data in backend')
      }
    ).catch(error => {
      console.log('Failed saving data')
      console.log(error)
      alert('Saving of license failed, please try again')
    })
  }

  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

}
