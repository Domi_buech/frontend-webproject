import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service'
import { User } from '../templates/user.template'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  loggedIn: boolean = false
  currentUser: User;
  currentUserId: number;

  constructor(private authService: AuthService) { 
    this.authService.currentUser.subscribe(x => 
      {
        if (x !== null) { 
          var myUser = JSON.parse(localStorage.getItem('currentUser'));
          this.currentUserId = myUser.oid
        }
      }
    )
  }

  ngOnInit(): void {
    if(JSON.parse(localStorage.getItem('currentUser'))) this.loggedIn = true
    else this.loggedIn = false
  }

}
