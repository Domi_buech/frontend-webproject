import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  companyList: string[];
  displayedColumns: string[] = ['oid', 'name', 'streetNr', 'street', 'postalCode', 'country', 'edit'];
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.loadAllCompanies()
  }

  loadAllCompanies() {
    console.log('CompaniesComponent loading data');
    this.api.getAllCompanies().then(
      apiValue => {
        this.companyList = apiValue
      }

    ).catch(error => {
      console.log('CompaniesComponent loading FAILED')
      console.log(error)
      alert('Loading companies failed, please try again or reload')
    })
  }


  deleteCompany(company) {
    console.log('deleteCompany()');
    this.api.deleteCompany(company).then(
      apiValue => {
        this.companyList = apiValue
        console.log(this.companyList)
      }
    ).catch(error => {
      console.log('Deleting company failed')
      console.log(error)
      alert('deleting company failed, please try again or reload')
    })
  }

}
