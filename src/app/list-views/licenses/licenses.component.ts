import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

import { License } from '../../templates/license.template'
import { Product } from '../../templates/product.template'

@Component({
  selector: 'app-licenses',
  templateUrl: './licenses.component.html',
  styleUrls: ['./licenses.component.css']
})
export class LicensesComponent implements OnInit {

  licenseList: License[];
  displayedColumns: string[] = ['oid', 'count', 'key', 'expires', 'ip1', 'ip2',
    'ip3', 'ip4', 'serconId', 'productId', 'edit'];

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.loadAllLicenses()
  }

  loadAllLicenses() {
    console.log('LicensesComponent loading data');
    this.api.getAllLicenses().then(
      apiValue => {
        this.licenseList = apiValue
        console.log(this.licenseList)
        for (let license of this.licenseList) {
          if (license.tproduct == null) {
            license.tproduct = new Product()
            license.tproduct.oid = null
          }
          if (license.tservicecontract_OID == 0) {
            license.tservicecontract_OID = null
          }
          
          if (license.tservicecontract_OID == 0) {
            license.tservicecontract_OID = null
          }
        }
      }

    ).catch(error => {
      console.log('LicensesComponent loading FAILED')
      console.log(error)
      alert('Loading licenses failed, please try again or reload')
    })
  }

  deleteLicense(license) {
    console.log('deleteLicense()');
    this.api.deleteLicense(license).then(
      apiValue => {
        this.licenseList = apiValue
        for (let license of this.licenseList) {
          if (license.tproduct == null) {
            license.tproduct = new Product()
            license.tproduct.oid = null
          }
          if (license.tservicecontract_OID == 0) {
            license.tservicecontract_OID = null
          }
          
          if (license.tservicecontract_OID == 0) {
            license.tservicecontract_OID = null
          }
        
        }
      }
    ).catch(error => {
      console.log('Deleting license failed')
      console.log(error)
      alert('deleting license failed, please try again or reload')
    })
  }

}
