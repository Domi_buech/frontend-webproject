import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

import { Company } from '../../templates/company.template'
import { ServiceContract } from '../../templates/serviceContract.template'

@Component({
  selector: 'app-servicecontracts',
  templateUrl: './servicecontracts.component.html',
  styleUrls: ['./servicecontracts.component.css']
})
export class ServicecontractsComponent implements OnInit {

  serconList: ServiceContract[];
  displayedColumns: string[] = ['oid', 'start', 'end', 'company', 'edit'];
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.loadAllServiceContracts()
  }

  loadAllServiceContracts() {
    console.log('ServiceContractComponent loading data');
    this.api.getAllServiceContracts().then(
      apiValue => {
        this.serconList = apiValue
        for (let sercon of this.serconList) {
          if (sercon.tcompany == null) {
            sercon.tcompany = new Company()
            sercon.tcompany.oid = null
          }
        }
      }

    ).catch(error => {
      console.log('ServiceContractsComponent loading FAILED')
      console.log(error)
      alert('Loading service contracts failed, please try again or reload')
    })
  }

  deleteServiceContract(sercon) {
    console.log('deleteLicense()');
    this.api.deleteServiceContract(sercon).then(
      apiValue => {
        this.serconList = apiValue
        for (let sercon of this.serconList) {
          if (sercon.tcompany == null) {
            sercon.tcompany = new Company()
            sercon.tcompany.oid = null
          }
        }
      }
    ).catch(error => {
      console.log('Deleting Service contract failed')
      console.log(error)
      alert('deleting Service contract failed, please try again or reload')
    })
  }

}
