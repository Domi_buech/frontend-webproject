import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { User } from '../../templates/user.template'
import { Company } from '../../templates/company.template'


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  userList: User[];
  displayedColumns: string[] = ['oid', 'loginname', 'active', 'admin', 'lastname',
    'firstname', 'email', 'company', 'edit'];
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.loadAllUsers()
  }

  loadAllUsers() {
    console.log('UsersComponent loading data');
    this.api.getAllUsers().then(
      apiValue => {
        this.userList = apiValue
        console.log(this.userList)

        // assign empty company for display
        for (let user of this.userList) {
          if (user.tcompany == null) {
            user.tcompany = new Company()
            user.tcompany.name = '-'
          }
        }
      }

    ).catch(error => {
      console.log('UsersComponent loading FAILED')
      console.log(error)
      alert('Loading users failed, please try again or reload')
    })
  }


  deleteUser(user) {
    console.log('deleteUser()');
    this.api.deleteUser(user).then(
      apiValue => {
        this.userList = apiValue
        console.log(this.userList)

        // assign empty company for display
        for (let user of this.userList) {
          if (user.tcompany == null) {
            user.tcompany = new Company()
            user.tcompany.name = '-'
          }
        }
      }

    ).catch(error => {
      console.log('Deleting user failed')
      console.log(error)
      alert('deleting user failed, please try again or reload')
    })
  }

}
