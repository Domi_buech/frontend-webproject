import { Component, OnInit } from '@angular/core';
import { User } from '../templates/user.template'
import { ApiService } from '../services/api.service';
import { AuthService } from '../services/auth.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(private router: Router, private apiService: ApiService, private authService: AuthService) { }

  enteredUser = new User();
  username: string;
  password: string;
  oid: number;
  companyOid: number
  isLoggedIn: boolean = false;
  admin: boolean;
  ngOnInit(): void {
  }


  async login() {
    console.log('LoginComponent loading data');
    this.enteredUser.loginname = this.username;
    this.enteredUser.password = this.password;
    console.log(this.enteredUser)

    await this.apiService.checkUserLogin(this.enteredUser).then(
      apiValue => {
        console.log(apiValue);
        if (apiValue.success == true) {
          this.oid = apiValue.oid
          this.companyOid = apiValue.tcompany_OID
          this.admin = apiValue.admin
          console.log(this.oid + ", " + this.companyOid + ',' + this.admin)
          apiValue['adminStatus'] = apiValue.admin
          console.log(apiValue)
          let verifiedUser = {'oid': this.oid, 'adminStatus': this.admin, 'tcompany_OID': this.companyOid}
          this.authService.loggedIn(verifiedUser)
          // this.router.navigate(['/profile/' + this.oid + '/' + this.companyOid])
          this.router.navigate(['/profile/'])
        } else if (apiValue.success == false) {
          console.log('Invalid credentials')
          alert("Invalid password");
        } else if (apiValue.notFound == true) {
          alert("You are not registered");
          console.log('User not found')
        }
      }

    ).catch(error => {
      console.log('Failed Loading Data')
      console.log(error)
    })
  }

}
