import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ApiService } from '../services/api.service';
import { AuthService } from '../services/auth.service'

import { User } from '../templates/user.template'
import { Company } from '../templates/company.template'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  user: User;
  companies: Company[]
  selectedCompany: Company;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.loadAllCompanies()
    this.user = new User();
    console.log('registerComponent: ngOnInit()')
  }

  async loadAllCompanies() {
    console.log('CompaniesComponent loading data');
    await this.apiService.getAllCompanies().then(
      apiValue => {
          this.companies = apiValue
        }
      
    ).catch(error => {
      console.log('CompaniesComponent loading FAILED')
      console.log(error)
    })
  }

  saveUser() {
    console.log(this.selectedCompany)
    this.user.tcompany = this.selectedCompany
    this.apiService.updateUser(this.user).then(
      apiValue => {
        console.log('Successfully saved user data in backend')
        let verifiedUser = {'oid': apiValue.oid, 'adminStatus': 0, 'tcompany_OID': this.selectedCompany.oid}
        this.authService.loggedIn(verifiedUser)
        this.router.navigate(['/home'])
      }
      ).catch(error => {
        console.log('Failed saving data')
        console.log(error)
        alert('Registering your profile has failed, please try again')
      })
  }

}
