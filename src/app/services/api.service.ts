import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const API_HTTP_LINK = "http://localhost:8080/webproject/rest"

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  httpHeaders = new HttpHeaders({
    'Content-type': 'application/json',
  })

  constructor(private http: HttpClient) { }


  checkUserLogin(user): Promise<any> {
    const promise = this.http.post(
      API_HTTP_LINK + '/users/login', user,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }


  // Getters
  getUserById(oid): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.http.get(
        API_HTTP_LINK + '/users/' + oid + '/',
        {
          headers: this.httpHeaders
        }
      ).subscribe((res: any) => {
        if (res == 'ERROR') {
          resolve([])
        }
        else {
          resolve(res.tuser)
        }
      })
    });
  }

  getCompanyById(oid): Promise<any> {
    const promise = this.http.get(
      API_HTTP_LINK + '/companies/' + oid,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  getCompanyEmployees(oid): Promise<any> {
    const promise = this.http.get(
      API_HTTP_LINK + '/companies/' + oid + '/employees',
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  getCompanyServiceContracts(oid): Promise<any> {
    const promise = this.http.get(
      API_HTTP_LINK + '/companies/' + oid + '/sercons',
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  getLicenseById(oid): Promise<any> {
    const promise = this.http.get(
      API_HTTP_LINK + '/licenses/' + oid,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  getServiceContractById(oid): Promise<any> {
    const promise = this.http.get(
      API_HTTP_LINK + '/sercons/' + oid,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  getAllUsers(): Promise<any> {
    const promise = this.http.get(API_HTTP_LINK + '/users/',
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }


  getAllCompanies(): Promise<any> {
    const promise = this.http.get(API_HTTP_LINK + '/companies/',
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  getAllServiceContracts(): Promise<any> {
    const promise = this.http.get(API_HTTP_LINK + '/sercons/',
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  getAllLicenses(): Promise<any> {
    const promise = this.http.get(API_HTTP_LINK + '/licenses/',
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }


  // post methods
  updateUser(user): Promise<any> {
    const promise = this.http.post(
      API_HTTP_LINK + '/users/' + user.oid, user,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  updateCompany(company): Promise<any> {
    const promise = this.http.post(
      API_HTTP_LINK + '/companies/' + company.oid, company,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  updateLicense(license): Promise<any> {
    license.expire = Date.parse(license.expire)
    const promise = this.http.post(
      API_HTTP_LINK + '/licenses/' + license.oid, license,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  updateServiceContract(sercon): Promise<any> {
    sercon.start = Date.parse(sercon.start)
    sercon.end = Date.parse(sercon.end)
    const promise = this.http.post(
      API_HTTP_LINK + '/sercons/' + sercon.oid, sercon,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  updateProduct(product): Promise<any> {
    const promise = this.http.post(
      API_HTTP_LINK + '/products/' + product.oid, product,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }


  //delete 
  deleteUser(user): Promise<any> {
    const promise = this.http.delete(
      API_HTTP_LINK + '/users/' + user.oid,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  //delete 
  deleteCompany(company): Promise<any> {
    const promise = this.http.delete(
      API_HTTP_LINK + '/companies/' + company.oid,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  //delete 
  async deleteLicense(license): Promise<any> {
    if (license.tproduct != null && license.tproduct.oid != null) {
      console.log(license.tproduct)
      console.log('deleteProduct')
      await this.deleteProduct(license.tproduct)
    }

    const promise = this.http.delete(
      API_HTTP_LINK + '/licenses/' + license.oid,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  //delete 
  deleteProduct(product): Promise<any> {
    const promise = this.http.delete(
      API_HTTP_LINK + '/products/' + product.oid,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

  //delete 
  deleteServiceContract(sercon): Promise<any> {
    const promise = this.http.delete(
      API_HTTP_LINK + '/sercons/' + sercon.oid,
      {
        headers: this.httpHeaders
      }
    ).toPromise()
    return promise
  }

}
