import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private apiService: ApiService, private router: Router,) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public getCurrentUserValue() {
    return this.currentUserSubject.value;
  }

  loggedIn(verifiedUser) {
    console.log('AuthService: loggedIn()');
    localStorage.setItem('currentUser', JSON.stringify(verifiedUser));
    console.log(verifiedUser)
    this.currentUserSubject.next(verifiedUser);
  }

  loggedOut() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  isAdmin(): Boolean {
    var myUser = JSON.parse(localStorage.getItem('currentUser'))
    try {
      if (!!myUser.adminStatus) return true
      else return false
    } catch (error) {
      console.log('not logged in at all')
      return false
    }
  }
}