import { User } from './user.template'
import { ServiceContract } from './serviceContract.template'

export class Company {
    oid: number
    name: string
    country: string
    department: string
    postalcode: number
    street: string
    streetnr: string
    tservicecontracts: ServiceContract[]
    tusers: User[]
}