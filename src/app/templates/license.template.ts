import { Product } from './product.template'

export class License {

    oid: number
    key: number
    expire: string
    ip1: string
    ip2: number
    ip3: string
    ip4: string
    licenseCount: number
    tproduct: Product
    tservicecontract_OID: number
    tservicecontract_TCOMPANY_OID: number
}