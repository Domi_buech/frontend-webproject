import { Company } from './company.template'

export class ServiceContract {

    oid: number
    start: string
    end: string
    tcompany: Company
    tcompany_OID: number
}