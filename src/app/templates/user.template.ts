import { Company } from './company.template'


export class User {
    oid: number
    active: number;
    admin: number;
    email: String;
    firstname: String;
    lastname: String;
    loginname: String;
    password: String;
    tcompany_OID: number
    tcompany: Company
}