import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service'

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  loggedIn: boolean = false
  admin: boolean = false;

  constructor(private router: Router, private authService: AuthService) {
    this.authService.currentUser.subscribe(x => {
      console.log('tooblar constructur')
      console.log(x)
      if (x !== null) {
        if (!!x.adminStatus == true) {
          this.admin = true
          console.log(this.admin)
          this.login()
        } else if (!!x.adminStatus == false) {
          this.admin = false
          this.login()
        }
      }
      else {
        this.loggedIn = false
        this.admin = false
      }
    }
    )
  }

  ngOnInit(): void {
    if (JSON.parse(localStorage.getItem('currentUser'))) this.loggedIn = true
    else this.loggedIn = false
    console.log(this.authService.currentUser)
  }

  login() {
    this.loggedIn = true
  }

  logout() {
    this.authService.loggedOut()
    this.loggedIn = false
    this.router.navigate([''])
  }

}
